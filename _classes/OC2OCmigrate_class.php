<?php
class OC2OCmigrate {
	public $query = "*";
	public $source_oc_url = "http://admin:admin@192.168.12.91:8080";
	public $destination_oc_url = "http://admin:admin@192.168.12.91:8080";
	public $candidates_subfolders = "GenericDate/metadata_mimetype";
	public $candidates_path = "/tmp/candidates/";
	public $candidates_done_path = "/tmp/candidates_done/";
	public $storage_path = "/tmp/storage/";
	public $uploaded_path = "/tmp/uploaded/";
	public $tmp_path = "/tmp/";
	public $axel_executable = "/usr/local/bin/axel";
	public $curl_executable = "/usr/bin/curl";
	public $verify_available_diskspace = true;
	public $verify_available_diskspace_array = array();
	private $error_message = "";
	public $verbose = false;
	
	public function __construct () {

    }

    public function get_migration_candidates () {
    	if (!is_writeable($this->candidates_path)) {
    		echo "ERROR: Candidates path (".$this->candidates_path.") is not writeable or does bot exist.\n";
    		die();
    	}

    	$data = file_get_contents($this->source_oc_url."/opencontent/basicsearch?q=".$this->query."&start=0&limit=1&property=uuid");

    	if ($data) {
    		$json = json_decode($data);

			$total_hits = $json->hits->totalHits;

			for ($i=0; $i<$total_hits;$i=$i+100) {
				echo "Requesting items ".$i." through ".($i+100)." out of ".$total_hits."\n";
				$data = file_get_contents($this->source_oc_url."/opencontent/basicsearch?q=".$this->query."&start=".$i."&limit=100");
				$this->parse_migration_candidates($data);
			}
    	} else {
    		echo "ERROR: failed to get data from source.\n";
    		die();
    	}
    }

    private function parse_migration_candidates ($data) {
    	$json = json_decode($data);
		$subfolder_array = explode("/", $this->candidates_subfolders);

		foreach ($json->hits->hits as $hit) {
			$uuid = $hit->id;
			$last_version = end($hit->versions);
			 
			$tmp_path = $this->candidates_path;

			foreach ($subfolder_array as $subfolder) {

				if (preg_match("/([^(]{1,})\(([0-9]{1,})\,([0-9]{1,})\)/", $subfolder, $matches)) {
					$tmp = end($last_version->properties->$matches[1]);
					$tmp = substr($tmp, $matches[2], $matches[3]);
				} else {
					$tmp = end($last_version->properties->$subfolder);
				} 
				
				$tmp_path .= preg_replace("/[^a-z0-9_\-\.\,\+]/i", "_", $tmp)."/";
				
				if (!is_dir($tmp_path)) {
					echo "\tCreate dir (".$tmp_path.")\n";
					mkdir($tmp_path);
				}
			}
			if(touch($tmp_path.$uuid)) {
				$this->filespace_trap();
				echo "\tCreated candidate ".$tmp_path.$uuid.".\n";	
			} else {
				echo "\tWARNING! Failed to created candidate ".$tmp_path.$uuid.".\n";
			}
		}
    }

    public function process_candidates ($directory = null) {
    	if (!is_writeable($this->candidates_done_path)) {
    		echo "ERROR: Candidates done path (".$this->candidates_done_path.") is not writeable or does not exist.\n";
    		die();
    	}

    	if (!is_writeable($this->storage_path)) {
    		echo "ERROR: Storage path (".$this->storage_path.") is not writeable or does not exist.\n";
    		die();
    	}
    		
		if (is_null($directory)) 
			$directory = $this->candidates_path;
    	
    	$DIR = opendir($directory);
    	$dir_array = array();

    	while (false !== ($tmp = readdir($DIR))) {
	 		if ($tmp && $tmp != ".DS_Store") {
	 			$dir_array[] = $tmp;
	 		}
	 	}

		foreach ($dir_array as $filename) {
			if ($filename != "." AND $filename != "..") {
				$filepath = $directory.$filename;
				
				if (is_file($filepath)) {
					$this->filespace_trap();
					$this->get_candidate_data($filepath);
				}  elseif (is_dir($filepath)) {
					$this->process_candidates($filepath."/");
    				@unlink ($filepath."/.DS_Store");
    				@rmdir($filepath);
				}
    		}
    	}
	}

	public function upload_packages ($directory = null) {
    	if (!is_readable($this->storage_path)) {
    		echo "ERROR: Storage path (".$this->storage_path.") is not readable or does not exist.\n";
    		die();
    	}

    	if (!is_writeable($this->uploaded_path)) {
    		echo "ERROR: Uploaded path (".$this->uploaded_path.") is not writeable or does not exist.\n";
    		die();
    	}
    		
		if (is_null($directory)) 
			$directory = $this->storage_path;

    	$DIR = opendir($directory);
    	$dir_array = array();

    	while (false !== ($tmp = readdir($DIR))) {
	 		if ($tmp && $tmp != ".DS_Store") {
	 			$dir_array[] = $tmp;
	 		}
	 	}

		foreach ($dir_array as $filename) {
			if ($filename != "." AND $filename != "..") {
				$filepath = $directory.$filename;
				
				if (is_dir($filepath)) {
					if (is_readable($filepath."/__META.json")) {
						$this->upload_package_to_oc($filepath);
					} else {
						$this->upload_packages($filepath."/");
    				}
				}
    		}
    	}
	}

	private function upload_package_to_oc ($path) {
		if (basename($path) != ".DS_Store") {
			$work_path = $this->tmp_path.basename($path);
			if (is_file($path."/__META.json")) {

				if (is_readable ($path)) {
					if (rename($path, $work_path)) {
						echo "Uploading package (".$path.")\n";
						
						$data = file_get_contents($work_path."/__META.json");

	    				if ($data) {
	    					$json = json_decode($data);

	    					$curl_string = $this->curl_executable." -m 500 -s -S ".escapeshellarg($this->destination_oc_url."/opencontent/objectupload");

	    					foreach ($json->metadata as $key => $metadata){
	    						if ($key == 0) {
	    							$meta_extension = "";
	    						} else {
	    							$meta_extension = $key+1;
	    						}

								$curl_string .= " -F metadata".$meta_extension."=".escapeshellarg($metadata->filename);
                    			$curl_string .= " -F metadata".$meta_extension."-mimetype=".escapeshellarg($metadata->mimetype);
                    			$curl_string .= " -F ".escapeshellarg($metadata->filename)."=@".escapeshellarg($work_path."/metadata_".$key);
							}

							if (!is_null($json->primary)) {
								$curl_string .= " -F file=".escapeshellarg($json->primary->filename);
                    			$curl_string .= " -F file-mimetype=".escapeshellarg($json->primary->mimetype);
                    			$curl_string .= " -F ".escapeshellarg($json->primary->filename)."=@".escapeshellarg($work_path."/primary");
							}

							if (!is_null($json->preview)) {
								$curl_string .= " -F preview=".escapeshellarg($json->preview->filename);
                    			$curl_string .= " -F preview-mimetype=".escapeshellarg($json->primary->mimetype);
                    			$curl_string .= " -F ".escapeshellarg($json->preview->filename)."=@".escapeshellarg($work_path."/preview");
							}

							if (!is_null($json->thumb)) {
								$curl_string .= " -F thumb=".escapeshellarg($json->thumb->filename);
                    			$curl_string .= " -F thumb-mimetype=".escapeshellarg($json->thumb->mimetype);
                    			$curl_string .= " -F ".escapeshellarg($json->thumb->filename)."=@".escapeshellarg($work_path."/thumb");
							}

							if ($this->verbose) {
								echo "\n=================\n".$curl_string."\n=================\n";
							}
							
							$uploadresult = "";
                			$uploadresult = shell_exec($curl_string); 
                
                			echo "\tRESPONSE: ".$uploadresult."\n";

                			if (preg_match("/[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}/", $uploadresult)) {
                    			echo "\tUPLOAD: OK\n";
                    
                    			// skapar mappar

								$array = explode("/", str_replace($this->storage_path, "", dirname ($path)));
					
								$done_path = $this->uploaded_path;
					
								foreach ($array as $tmp) {
									$done_path .= $tmp."/";
								 
									if (!is_dir($done_path)) {
										echo "\tCreate dir (".$done_path.")\n";
										mkdir($done_path);
									}
								}

								if (!rename ($work_path, $done_path.basename($path))) {
									echo "WARNING! failed to rename ".$work_path." to ".$done_path.basename($path).".\n";
								}	
                    
                			} else {
                    			echo "\tWARNING: Upload failed.\n";
                    			/*if (rename($work_path, $path)) {
                    				echo "\tNOTICE: renamed tmp-path (".$work_path.") back to storage path (".$path.").\n";
                    			}*/
                			}

	    				} else {
	    					echo "\tWARNING! Failed to read metadata-file (".$path."/__META.json).\n";
	    				}
					} else {
						echo "WARNING! failed to rename ".$path." to ".$work_path.".\n"; 
					}
				} else {
					echo "WARNING! Unable to read (".$path."). Possibly processed by other\n";
				}
			} else {
				// already processed
			}	
		}
	}
	
	private function download_file ($url, $destination) {
		shell_exec($this->axel_executable." -o ".escapeshellarg($destination)." ".escapeshellarg($url));

		if (is_readable($destination)) {
			echo "\t\t\tDownloaded and created file (".$destination.")\n";
			return true;
		} else {
			echo "\t\t\tWARNING! Failed to download and create file (".$destination.")\n";
			return false;
		}
	}

	private function get_candidate_data ($path) {
		if (basename($path) != ".DS_Store") {
			$work_path = $this->tmp_path.basename($path);
			if (is_file($path)) {
				if (is_readable ($path)) {
					if (rename($path, $work_path)) {
						echo "Processing candidate (".$path.")\n";
						// skapar mappar

						$array = explode("/", str_replace($this->candidates_path, "", dirname ($path)));
					
						$done_path = $this->candidates_done_path;
						$out_path = $this->storage_path;
					
						foreach ($array as $tmp) {
							$done_path .= $tmp."/";
							$out_path .= $tmp."/";
							 
							if (!is_dir($done_path)) {
								echo "\tCreate dir (".$done_path.")\n";
								mkdir($done_path);
							}

							if (!is_dir($out_path)) {
								echo "\tCreate dir (".$out_path.")\n";
								mkdir($out_path);
							}
						}

						$object_path = $out_path.basename($path);
						echo "\tCreate dir (".$object_path.")\n";
						mkdir ($object_path);
						
						// processar
						echo "\t\tGetting data for ".basename($path)."\n";
						
						$base_url = $this->source_oc_url."/opencontent/objects/".basename($path)."/files";
						
						$data = file_get_contents($base_url);

	    				if ($data) {
	    					$json = json_decode($data);

	    					foreach ($json->metadata as $key => $metadata){
								$this->download_file($base_url."/".$metadata->filename, $object_path."/metadata_".$key);
							}

							if (!is_null($json->primary)) {
								$this->download_file($base_url."/".$json->primary->filename, $object_path."/primary");
							}

							if (!is_null($json->preview)) {
								$this->download_file($base_url."/".$json->preview->filename, $object_path."/preview");
							}

							if (!is_null($json->thumb)) {
								$this->download_file($base_url."/".$json->thumb->filename, $object_path."/thumb");
							}

							file_put_contents($object_path."/__META.json", $data);
	    				} else {
	    					echo "\tWARNING! Failed to retrieve candidate data (".basename($path).").\n";
	    					echo "\tDeleting dir (".$object_path.")\n";
	    					rmdir($object_path);
	    				}
						
						if (!rename ($work_path, $done_path.basename($path))) {
							echo "WARNING! failed to rename ".$work_path." to ".$done_path.basename($path).".\n";
						}	
					} else {
						echo "WARNING! failed to rename ".$path." to ".$work_path.".\n"; 
					}
				} else {
					echo "WARNING! Unable to read (".$path."). Possibly processed by other\n";
				}
			} else {
				// already processed
			}	
		}
	}

	private function filespace_trap() {
		if (!$this->verify_diskspace()) {
			$alert_loop_conter = 0;
			while (!$this->verify_diskspace()) {
				if ($alert_loop_conter >= 10) {
					$this->feedback("DISK ERROR", $this->error_message, true);
					$alert_loop_conter = 0;
				} else {
					$this->feedback("DISK ERROR", $this->error_message, false);
					$alert_loop_conter++;
				}

				echo "\nNOTICE! Sleeping for 30 seconds.\n";
				sleep(30);
			}
		}

		return true;
	}

	private function verify_diskspace() {
		if ($this->verify_available_diskspace) {
			if (count($this->verify_available_diskspace_array) > 0) {
				foreach ($this->verify_available_diskspace_array as $volume => $disk) {
					if (!$this->enough_diskspace_left($disk["path"], $disk["minimum"])) {
						$this->error_message = "Out of disk space (".$volume.") (".$this->name.").";
						return false;
					}
				}
			}
		}

		return true;
	}

	public function add_diskspace_watcher($name, $path, $minimum) {
		$this->verify_available_diskspace_array[$name] = array("path" => $path, "minimum" => $minimum);
	}

	private function enough_diskspace_left($path, $limit) {
		$disk_free_space = disk_free_space($path);
		$disk_total_space = disk_total_space($path);

		if (preg_match("/([0-9]{1,})[ ]{0,}TB/i", $limit, $matches)) {
			// TB
			if (($matches[1]*1024*1024*1024*1024)<=$disk_free_space) {
				return true;
			} else {
				return false;
			}
		} elseif (preg_match("/([0-9]{1,})[ ]{0,}GB/i", $limit, $matches)) {
			// GB
			if (($matches[1]*1024*1024*1024)<=$disk_free_space) {
				return true;
			} else {
				return false;
			}
		} elseif (preg_match("/([0-9]{1,})[ ]{0,}MB/i", $limit, $matches)) {
			// MB
			if (($matches[1]*1024*1024)<=$disk_free_space) {
				return true;
			} else {
				return false;
			}
		} elseif (preg_match("/([0-9]{1,})[ ]{0,}KB/i", $limit, $matches)) {
			// KB
			if (($matches[1]*1024)<=$disk_free_space) {
				return true;
			} else {
				return false;
			}
		} elseif (preg_match("/([0-9]{1,})[ ]{0,}\%/i", $limit, $matches)) {
			// %
			if ($matches[1]<=($disk_free_space/$disk_total_space*100)) {
				return true;
			} else {
				return false;
			}
		} elseif (preg_match("/([0-9]{1,})[ ]{0,}[B]{0,1}/i", $limit, $matches)) {
			// B
			if (($matches[1])<=$disk_free_space) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	private function feedback($title, $message, $tweet=false) {
		echo "\n\n".$title." : ".$message."\n\n";
		if ($tweet) {
			$this->notify($title, $message);
		}
	}

	private function notify($title, $message) {
 		exec ("/usr/bin/osascript -e 'display notification \"".$message."\" with title \"".$title."\"' ");
 		exec("/usr/local/bin/ttytter -keyf='".ROOT_PATH."_system/includes/.ttytterkey' -status='$message' & ");
 	}
}

